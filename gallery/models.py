from random import randint
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models

# Create your models here.
from django.db.models import Sum, F
from django.forms import ModelForm
from django.utils.baseconv import base56

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill


class Picture(models.Model):
    image = models.ImageField()
    description = models.TextField(blank=True)
    slug = models.SlugField(unique=True)
    dateLoad = models.DateTimeField(auto_now_add = True, editable= False)
    lastShow = models.DateTimeField(blank = True, editable= True, null=True)
    counter = models.PositiveIntegerField(default=0)
    owner = models.ForeignKey(User, blank=True, null=True)
    curent_rating = models.PositiveIntegerField(default=0)
    rewies = models.PositiveIntegerField(default=0)

    image_thumbnail = ImageSpecField(source='image',
                                      processors=[ResizeToFill(320, 200)],
                                      format='JPEG',
                                      options={'quality': 60})
    image_carusel = ImageSpecField(source='image',
                                      processors=[ResizeToFill(800, 300)],
                                      format='JPEG',
                                      options={'quality': 100})


    def get_absolute_url(self):
        return reverse('detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        if self.slug == '':
            self.slug = base56.encode(randint(0, 0x7fffffff))
        super(Picture, self).save(*args, **kwargs)



    def __str__(self):
        return '{} {}'.format(self.slug , self.image)

    def incrementCounter(self):
        self.counter = F('counter') + 1

    def setRewiew(self, rate, user):
        obj, create = rating.objects.get_or_create(picture = self, user=user, defaults={'rating': rate})
        if not create:
            obj.rating = rate
            obj.save()
        self.curent_rating = rating.getRatingByPicture(self)

    @classmethod
    def getLastUploded(cls):
        obj = cls.objects.order_by('dateLoad')[:12]
        # obj =
        return  obj

    @classmethod
    def getCarusel(cls):
        obj = cls.objects.order_by('counter')[:3]
        # obj =
        return  obj

    @classmethod
    def getPopular(cls):
        obj = cls.objects.order_by('counter')[:12]
        # obj =
        return  obj

    @classmethod
    def getBySlug(cls, slug):
        obj = cls.objects.get(slug = slug)
        # obj =
        return  obj


class rating(models.Model):
    picture = models.ForeignKey(Picture)
    user = models.ForeignKey(User)
    rating = models.PositiveIntegerField()


    class Meta:
        unique_together = ('picture', 'user')

    @classmethod
    def getRatingByPicture(cls, picture):
        obj = cls.objects.filter(picture = picture)
        ratingSum = obj.annotate(Sum('rating'))
        rating = ratingSum // obj.count()
        return rating

    @classmethod
    def getRatingCountPicture(cls, picture):
        obj = cls.objects.filter(picture = picture)
        return obj.count()

class pictureForm(ModelForm):
    class Meta:
        model = Picture
        fields = ('image', 'description')