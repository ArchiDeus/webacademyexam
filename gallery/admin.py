from django.contrib import admin

# Register your models here.
from gallery.models import Picture, rating


class PictureAdmin(admin.ModelAdmin):
    readonly_fields = ('curent_rating','rewies', 'counter', 'slug')
    def get_form(self, request, obj=None, **kwargs):
        if request.user.is_superuser:
            self.exclude = ()
        else:
            self.exclude = ('slug',)
        return super(PictureAdmin, self).get_form(request, obj=None, **kwargs)


admin.site.register(Picture, PictureAdmin)
admin.site.register(rating)