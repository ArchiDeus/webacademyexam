from datetime import datetime
from django.db.models import F
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import TemplateView
from gallery.models import pictureForm, Picture


class mainPage(TemplateView):
    template_name = 'index.html'
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['form'] = pictureForm()
        last12 = Picture()
        context['pictures'] = last12.getLastUploded()
        context['top_like'] = Picture.getCarusel()
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = pictureForm(request.POST, request.FILES)
        if form.is_valid():
            picture = Picture(image = request.FILES['image'])
            picture.save()
            return HttpResponseRedirect('/'+picture.slug)
        context['form'] = form
        last12 = Picture()
        context['pictures'] = last12.getLastUploded()
        context['top_like'] = Picture.getCarusel()
        return self.render_to_response(context)

    # if request.method == 'POST':
    #     form = UploadFileForm(request.POST, request.FILES)
    #     if form.is_valid():
    #         instance = ModelWithFileField(file_field=request.FILES['file'])
    #         instance.save()
    #         return HttpResponseRedirect('/success/url/')
    # else:
    #     form = UploadFileForm()
    # return render(request, 'upload.html', {'form': form})

class ImageDetailView(TemplateView):
    template_name = 'detail.html'
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        try:
            picture = Picture.objects.get(slug = context['slug'])
            picture.count = F('count') +1
            picture.lastShow = datetime.today()
            picture.save()
        except Picture.DoesNotExist:
            return redirect('/')

        context['picture'] = picture
        return self.render_to_response(context)


class PopularView(TemplateView):
    template_name = 'popular.html'
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        picture = Picture.getPopular()
        context['pictures'] = picture
        return self.render_to_response(context)