# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0009_auto_20160821_2115'),
    ]

    operations = [
        migrations.AddField(
            model_name='picture',
            name='thumbnail',
            field=models.ImageField(null=True, upload_to='', blank=True, max_length=500),
        ),
    ]
