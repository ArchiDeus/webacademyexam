# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0006_auto_20160818_1638'),
    ]

    operations = [
        migrations.AddField(
            model_name='picture',
            name='curent_rating',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='picture',
            name='rewies',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
