# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='picture',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('image', models.ImageField(upload_to='')),
                ('description', models.TextField()),
                ('key', models.SlugField(unique=True)),
                ('dateLoad', models.DateField()),
                ('lastShow', models.DateTimeField()),
                ('counter', models.PositiveIntegerField(default=0)),
            ],
        ),
    ]
