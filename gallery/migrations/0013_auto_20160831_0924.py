# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0012_auto_20160821_2140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='picture',
            name='lastShow',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
