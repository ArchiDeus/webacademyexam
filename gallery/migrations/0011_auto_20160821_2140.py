# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0010_picture_thumbnail'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='picture',
            name='thumbnail',
        ),
        migrations.AlterField(
            model_name='rating',
            name='picture',
            field=models.ForeignKey(to='gallery.Picture'),
        ),
    ]
