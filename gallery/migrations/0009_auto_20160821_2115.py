# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0008_auto_20160821_2101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rating',
            name='picture',
            field=models.ForeignKey(to='gallery.Picture'),
        ),
    ]
