# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0005_auto_20160818_1634'),
    ]

    operations = [
        migrations.AlterField(
            model_name='picture',
            name='dateLoad',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='picture',
            name='lastShow',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
