# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0003_rating'),
    ]

    operations = [
        migrations.AddField(
            model_name='rating',
            name='rating',
            field=models.PositiveIntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='rating',
            unique_together=set([('picture', 'user')]),
        ),
    ]
